# Erste Schritte

Beginnen wir mit Deiner DCES-Reise! Es gibt viel zu erlernen, fangen wir einfach mal anfangen.
In diesem Kapitel werden wir folgendes erörtern:

* Die Installation von DCES auf Linux, BSD, macOS und Windows.
* Die Benutzung von `cargo`, Rusts Abhängigkeiten-, Komponentenmanager und  Build-System.
