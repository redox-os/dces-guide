# Appendix D: Handbuch Übersetzungen

Quellen zu Übersetzungen in andere Sprachen. Bitte nutze diesen
[Übersetzungs-Link][label], wenn Du selber aktiv beitragen möchtest,
oder uns über neue Übersetzungen zu informieren. Danke!

- [English](https://gitlab.redox-os.org/redox-os/dces-rust/guide/-/tree/main/src/en)
- [Deutsch](https://gitlab.redox-os.org/redox-os/dces-rust/guide/-/tree/main/src/de)

[label]: https://gitlab.redox-os.org/redox-os/dces-rust/issues?q=is%3Aopen+is%3Aissue+label%3ATranslations

<!--
- [Deutsch](https://gitlab.redox-os.org/redox-os/dces-rust/guide/-/tree/main/src/de)
- [Español](https://gitlab.redox-os.org/redox-os/orbtk-book/-/tree/main/src/es)
- [Français](https://gitlab.redox-os.org/redox-os/orbtk-book/-/tree/main/src/fr)

[label]: https://gitlab.redox-os.org/redox-os/dces-rust/issues?q=is%3Aopen+is%3Aissue+label%3ATranslations

-->
