# DCES Examples - Minimal

This subsection documents the example application **dces_minimal**.

In a minimalistic use-case, this example creates a single **world**. It
creates one **entity** and assignes one **component** that provides
the property "name".

A print **system** will queries into the entity store "e_store",
collects the given entity and prints out the "value" of any component
that has a matching component with the property key "name". Without
any fancy magic, they are printed to stdout.

## Update Cargo.toml

First have a look at the corresponding *Cargo.toml* file as shown in Listing 1-1.

```toml,ignore
{{#include ./listings/ch09-02-dces-minimal/listing-02-01/Cargo.toml}}
```

<span class="caption">Listing 1-1: Projects metadata "dces_minimal"</span>

## Program source code

All of the **DCES** specific code that is needed to build the **dces_minimal**
example is shown in [Listing 1-2][dces_minimal, that is encode in
*src/main.rs*.

Enter the following commands inside the terminal window to compile and
run in debug mode:

```console
$ cargo run --example miminal
```

The following output should be printed inside your console window:

```console
DCES
```

[dces_minimal]: #complete-example-source

## Recap and annotation

### The anatomy of the `dces_minimal` application

Let’s review the relevant parts of the **dces_minimal** application.
A more in depth view of a typical `DCES` application is documented
inside the annotated [dces_basic][dces_basic_source] source.

[dces_basic_source]: ./ch09-01-dces-basic.md

Following code block extracts the implementation of the PrintSystem:

```rust,ignore
{{#include ./listings/ch09-02-dces-minimal/listing-02-01/src/main.rs:Implement_PrintSystem}}
```

We loop over the given entity store "e_store" and get any component
with a key matching `"name"`. If the value us non-null, it will be
printed to stdout.

```rust,ignore
{{#include ./listings/ch09-02-dces-minimal/listing-02-01/src/main.rs:Query_EntityStore}}
```

As a result, the string `DCES!` will make it as the console output.

### Complete example source

Find attached the complete source code for our **dces_minimal** example.

```rust,ignore
{{#rustdoc_include ./listings/ch09-02-dces-minimal/listing-02-01/src/main.rs:All}}
```

<span class="caption">Listing 1-2: dces_minimal - Create a World, an entities, and a print systems.</span>

### Compiling and Running Are Separate Steps

The **cargo** call to compile `dces_minimal` will place the resulting
binary in the target subfolder of the project.

```console
$ cargo build --release --bin dces_minimal.rs
$ ../target/release/dces_minimal
```

On Windows, you need to use `backslash` as a path delimiter:

```powershell
> cargo build --release --bin dces_minimal.rs
> ..\target\release\dces_minimal.exe
```
