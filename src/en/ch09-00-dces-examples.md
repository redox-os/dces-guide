# DCES Example Applications

This section provides `DCES` example apps. We hope to cover
interesting aspects.

Take them as a tutorial, all listings are created as a reference. They
have in mind to serve as an introduction to a specific topic. As
educational content, this apps are marked with in-lined comments and
anchors. If we did well, you can concentrate on the parts we like
to emphasize.

Inside the library, you will find the collection of example code in
the sub-directory [`examples`][examples].

The annotated sources are placed in the subdirectory
[`listings`][listings]. A Cargo.toml will group all apps members as a workspace.
Change inside the listings root and call:

```cargo
cargo build
```

That will build them all in one run using debug mode. To run a
specific app, e.g. call:

```cargo
cargo run --bin dces_resource
```

[examples]: https://gitlab.redox-os.org/redox-os/dces-rust/tree/develop/examples
[listings]: https://gitlab.redox-os.org/redox-os/dces-guide/tree/main/src/listings
