# DCES Beispiel - Resource

Dieser Abschnitt dokumentiert die Beispeilanwendung **dces_resource**.

Erneut erstellen wir nur eine **world** Instanz. Anschließend wir eine
**entity** erzeugt, der wir eine **component**zuweisen. Diese
Komponente stellt eine Eigenschaft "name" bereit. Ebenso wird eine
veränderbare **resource**, die den den Typ "HelloDCES" verwendet
zugeordnet.  Im **print system** stellen wir sicher, dass jede
Eigenschaft deren Komponenten-Wert dem Schlüssel "name" entspricht
ausgegeben wird.  Zusätzlich wird die Methode "say_hello()" der
Ressource "HelloDCES" ausgeführt. Dies stellt als Ergebnis die
Zeichenfolge "Hello DCES!" bereit, die ebenfalls im Konsolen-Fenster
ausgegeben wird.

## Überarbeitung Cargo.toml

Werfe zunächst einen Blick auf die zugehörige *Cargo.toml* Datei, die
in Listing 1-1 ausgebeben wird.

```toml,ignore
{{#include ./listings/ch09-03-dces-resources/listing-03-01/Cargo.toml}}
```

<span class="caption">Listing 1-1: Projek Metadata "dces_resources"</span>

## Der Program-Quellcode

Der gesamte **DCES** spezifische Quellcode der erforderlich ist um
unser Beispiel **dces_resources** zu erzeugen, findest du im
[Listing 1-2][dces_resource], das in der Datei *src/main.rs* kodiert wird.

Gib den folgenden Befehl ein, um den Kopilierungslauf zu starten und
anschließend die erzeugte App im debug Modus zu starten:

```console
$ cargo run --example resource
```

Nachfolgende Ausgabe sollte im Konsolen-Fenster ausgegeben werden:

```console
DCES
Hello DCES!
```

[dces_resource]: #complete-example-source

## Zusammenfassung und Anmerkungen

### Die Anatomie der `dces_resource` Anwendung

Lass uns die Details durchsehen, die innerhalb unserer
**dces_resource** App umgesetzt wurden.  Eine tiefergehende Darstellung
einer typischen `DCES` Anwendung findest Du innerhalb der annotierten
Quelle [dces_basic][dces_basic_source].

[dces_basic_source]: ./ch09-01-dces-basic.md

Der foldene Quellcode block schneidet die Definition des **PrintSystems** heraus:

```console
{{#include ./listings/ch09-03-dces-resources/listing-03-01/src/main.rs:Implement_PrintSystem}}
```

Der Wert der Komponente mit dem Schlüssel `"name"` wird wie bereits
besprochen und dokumentiert verarbeitet.  Der uns interessierende neue
Teil liegt in der Verarbeitung der Resource-Struktur über des
Druck-System.

```console
{{#include ./listings/ch09-03-dces-resources/listing-03-01/src/main.rs:StructResource}}
```

Der Quellcode implementiert den Typ **HelloDCES**, der die Methode **say_hello()** bereitstellt.

```console
{{#include ./listings/ch09-03-dces-resources/listing-03-01/src/main.rs:Implement_Resource}}
```

Mit der Methode `get()` des Typs Ressource verketten wir den Aufruf
zur `say_hello()` Methode. Diese gibt die **Zeichenkette**
zurück. Diese wird ihrerseits and das Macro **println!()** übergeben.

```console
{{#include ./listings/ch09-03-dces-resources/listing-03-01/src/main.rs:Print_StructResource}}
```

Als Resultat wird die Zeichenkette `Hello DCES!` im Konsolen-Fenster ausgegeben.

### Vollständiger Quellcode des Beispiels

Im Listing wird der vollständige Quellcode des **dces_resource** Beispiels aufgeführt.

```rust,ignore
{{#rustdoc_include ./listings/ch09-03-dces-resources/listing-03-01/src/main.rs:All}}
```

<span class="caption">Listing 1-2: dces_resource - Create a World, its entities, resources and a print systems.</span>

### Kompilierung und Aufruf im zwei Schritten

Die mit **cargo** übersetzte App `dces_resource` wird im
Unterverzeichnis `target` des Projekt-Wurzelverzeichnis abgespeichert.

```console
$ cargo build --release --bin dces_resource.rs
$ ../target/release/dces_resource
```

Auf Windows Systemen musst Du `backslash` als Pfad Trennzeichen nutzen:

```powershell
> cargo build --release --bin dces_resource.rs
> ..\target\release\dces_resource.exe
```
