# Einführung

[<img src="img/dces_logo_dark.png" width="720"/>](img/dces_space.png)

Willkommen zum **Dense Component Entity System**, ein Leitfaden zu
`DCES`.  Im archetype, bzw. kompakten ECS Modell werden die Elemente
(`entities`) in Tabellenform gespeichert.  Komponenten (`components`)
bilden die Spalten und Elemente bilden die Zeilen der
Tabelle. Archetypische Implementierungen bieten als Vorteil eine
schnelles Auffinden und Durchsuchen von Einträgen.

Dieser Leitfaden bietet einen Überblick der unterschiedenen Merkmale
und das verwendete Konzept von `DCES`. Zusätzlich wird über Beispiele
deren Verwendung kommentiert. Wir hoffen, es erleichtert Dir die
Einarbeitung und bietet darüber hinaus ein vertiefendes Verständnis im
Umgang mit `DCES`.

<div class="warning">

Warnung: Dieser Leitfaden ist nicht vollständig. Alles zu
dokumentieren und überarbeitete Teils zu aktualisieren dauert eine
Weile. Bitte schaue in die [Fehlerverfolgung][issue tracker], um zu
überprüfen was fehlerhaft oder unvollständig ist. Wenn Du Dinge
findest, die noch nicht gemeldet wurden oder Deiner Meinung nach
fehlen, stelle bitte einfach ein neues Ticket ein.

</div>

[issue tracker]: https://gitlab.redox-os.org/redox-os/dces-rust/-/issues

<!--
[issue tracker]: https://gitlab.redox-os.org/redox-os/dces-rust/-/issues
-->

## Merkmale

* Registriert Elemente und Komponenten.
* Teilt Komponenten zwischen Elementen.
* Registriert Systeme und liest / schreibt Komponenten für Elemente.
* Bearbeitet Systeme ensprechend ihrer Prioritäten.
* Registriert Container zur organisation von Elementen (Vec, FxHashMap, eigene Container, ...).
* Registriert das Initialisierungs- und Bereinigung Sub-System.

## Für wen ist DCES

`DCES` passt in Projekten, bei denen die Programmierer die Vorteile
der Progammiersprache Rust bei der Erstellung von Spielen oder unter
Verwendung von Frameworks für Grafischen-Oberflächen nutzen wollen.
Weil alles nativ in Rust erstellt vermeiden wir die Transformation von
Daten-Strukturen und Typen. Als Beispiel sei auf `OrbTk` verwiesen,
das vollständig auf dieses Variante eines Entity Component System
(ECS) aufbaut.

Es gibt eine ganze Reihe von Gründen, warum ein ECS sich gerade bei Spiele-Entwicklern steigender Popularität erfreut:

	* Ein ECS kann tpyischerweise eine sehr große Anzahl von Spiel-Objekten verwalten

	* Ein ECS eröffnet häufig ein vereinfachte Code Wiederverwendung
	* Ein ECS kann leichter um neue Merkmale erweitert werden
	* Ein ECS erlaubt die Umsetzung eines dynamischeren Codierungs-Stils


## Warum nicht einfach ... nutzen

Vielleicht stellst Du dir gerade die Fragen, warum Du nicht einfach
ein anderes ECS nutzen solltest, das für eine Produktion tauglich,
aktiv betreut und mit einem vollständigen Funktionsumfang versehen
ist? Und Du hast Recht. Nutze Deine Freiheit, denn es gibt mindestens
zwei ECS `crates`, die als leistungsstarke Rust-Alternativen neben
`DCES` genannt werden sollten.

`DCES` primäres Ziel ist die Bereitstellung eines **schnellen**,
**möglichst frei von Abhängigkeiten** ECS mit einer Vollständigen
Integration in das Orbital Toolkit ([OrbTK][orbtk])
bereitzustellen. Wir haben über die Einbindung eines ECS System mit
der Option einer parallelisierten Verarbeitung in `DCES` nachgedacht.
Bis auf Weiteres wurde dies Option verworfen, da mit einer
parallelisierten Verarbeitung in einem ECS immer auch ein Overhead
eingeführt wird.

Du solltest als bei der Implementierung deines Projekts prüfen, ob der
Wechsel für Dich Vorteile einbringt. Wenn Die Anzahl der zu
iterierenden Elemente nicht zu groß ist, ist eine sequentielle
Verarbeitung letztlich effizienter und schneller.

[orbtk]: https://github.com/redox-os/orbtk

### Specs
Komponenten und Systeme in Specs können auch parallel verarbeitet
werden. Specs wird als sogenannter **Dispatcher** verwendet, um dies
umzusetzen.  Details findest Du im online [Tutorial][specs_tutorial].

[specs_tutorial]: https://specs.amethyst.rs/docs/tutorials/

### legion
Das **legion** ECS implementiert ebenfalls ein archetypisches Speicher
Layout. Es hält **trait-less** Komponenten vor.

### Shipyard
Es handelt sich um ein **sparse set** basiertes ECS. Es speichert alle
Komponenten in einem dedizierten sparse set, bei dem jede id eines
Elements gleichzeitig auch der Schlüssel zur zugehörigen Komponente
ist. Der Vorteil von sparse-set Implementierungen liegt u.a. im sehr
schnellen Hinzufügung und Löschen.

## Quellcode

Der Quellcode zur Erstellung diese Leitfadens wird über die Homepage [DCES guide (en)][dces_guide_en] bereigestellt.

[dces_guide_en]: https://gitlab.redox-os.org/redox-os/dces-rust.git

<!--
[dces_guide_en]: https://github.com/redox-os/dces-rust/tree/main/guide/src/en
-->
