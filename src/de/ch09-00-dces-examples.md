# DCES - Beispiel-Anwendungen

Dieser Abschnitt werden die verfügbaren `DCES` Beispielprogramme vorgestellt.
Wir hoffen, dass sie interessante Aspekte der Bibliothek behandeln.

Die Beispiele verstehen sich als Anleitung, alle Quellcode-Listen sind
als Referenzen eingefügt. Sie eignen sich als Einführung in ein
bestimmtes Thema innerhalb von `DCES`. Als Lerninhalte sind diese
Apps mit Kommentaren und Ankern versehen. Wenn wir gut gearbeitet
haben, heben wir die Teile hervor, auf die Sie sich im jeweiligen Beispiel konzentrieren können.

Innerhalb der Bibliothek finden Sie die Sammlung der Quellen im
Unterverzeichnis [`Beispiele`][examples].

Die annotierten Quellen befinden sich um Unterverzeichnis
[`Listings`][listings].

Eine `Cargo.toml` Datei gruppiert alle Quellen als `workspace`.
Wechsle in das "listings" Root-Verzeichnis und starte den Compile-Lauf aller apps mit:

```cargo
cargo build
```

Willst Du eine bestimmte app ausführen, kannst Du diese Binärdatei mit folgendem Aufruf erzeugen:

```cargo
cargo run --bin dces_resource
```

[examples]: https://gitlab.redox-os.org/redox-os/dces-rust/guide/tree/main/examples
[listings]: https://gitlab.redox-os.org/redox-os/dces-guide/tree/main/src/listings
