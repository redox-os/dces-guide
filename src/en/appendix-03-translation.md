# Appendix D: Guide translations

For resources in languages other than English. This is work in progress; see
[the Translations label][label] to help or let us know about a new translation!

- [Deutsch](https://gitlab.redox-os.org/redox-os/dces-rust/guide/tree/main/src/de)

[label]: https://gitlab.redox-os.org/redox-os/dces-rust/issues?q=is%3Aopen+is%3Aissue+label%3ATranslations

<!--
- [Español](https://gitlab.redox-os.org/redox-os/dces-rust/guide/tree/main/src/es)
- [Français](https://gitlab.redox-os.org/redox-os/dces-rust/guide/tree/main/src/fr)
-->
