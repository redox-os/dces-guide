# DCES Beispiel - Minimal

Dieser Abschnitt dokumentiert die Beispielanwendung **dces_minimal**.

Dieser Abschnitt dokumentiert die Beispielanwendung **dces_shared**.

In einem minimale Anwendungsumfang erstellt das Beispiel eine
**world**. Hier wird einer **entity** erzeugt und dieser eine
**component** zugewiesen. Die Komponente stellt die Eigenschaft "name"
bereit.

Ein **print system** steuert eine Abfrage in den
Eigenschaften-Speicher "e_store", wählt die gewünschten Eigenschaften
und druckt die "Werte" der Bestandteile mit dem Schlüsselwert
"name". Die verwendete Logik ist simpel: Gib den Eigenschafts-Namen
auf der Konsole aus (stdout).

## Überarbeitung Cargo.toml

Werfe zunächst einen Blick auf die zugehörige *Cargo.toml* Datei, die
in Listing 1-1 ausgebeben wird.

```toml,ignore
{{#include ./listings/ch09-02-dces-minimal/listing-02-01/Cargo.toml}}
```

<span class="caption">Listing 1-1: Projekt Metadata "dces_minimal"</span>

## Der Program-Quellcode

Der gesamte **DCES** spezifische Quellcode der erforderlich ist um
unser Beispiel **dces_minimal** zu erzeugen, findest du im
[Listing 1-2][dces_minimal], das in der Datei *src/main.rs* kodiert wird.

Gib den folgenden Befehl ein, um den Kopilierungslauf zu starten und
anschließend die erzeugte App im debug Modus zu starten:

```console
$ cargo run --example miminal
```

Nachfolgende Ausgabe sollte im Konsolen-Fenster ausgegeben werden:

```console
DCES
```

[dces_minimal]: #complete-example-source

## Zusammenfassung und Anmerkungen

### Die Anatomie der `dces_minimal` Anwendung

Lass uns die Details durchsehen, die innerhalb unserer
**dces_minimal** App umgesetzt wurden.  Eine tiefergehende Darstellung
einer typischen `DCES` Anwendung findest Du innerhalb der annotierten
Quelle [dces_basic][dces_basic_source].

[dces_basic_source]: ./ch09-01-dces-basic.md

Der foldene Quellcode Block schneidet die Definition des **PrintSystems** heraus:

```rust,ignore
{{#include ./listings/ch09-02-dces-minimal/listing-02-01/src/main.rs:Implement_PrintSystem}}
```

Mit einer Schleife verarbeiten wir die **entities** des relevanten
"e_store" und iterieren über alle Komponenten, deren Schlüssel
 `"name"` entsprechen. Ist deren Wert *non-null* wird dieser in im Konsolenfenster ausgegeben (stdout).

```rust,ignore
{{#include ./listings/ch09-02-dces-minimal/listing-02-01/src/main.rs:Query_EntityStore}}
```

Als Ergebins erhalten wir die Zeichenfolge `DCES!`.

### Vollständiger Quellcode des Beispiels

Im Listing wird der vollständige Quellcode des **dces_minimal** Beispiels aufgeführt.

```rust,ignore
{{#rustdoc_include ./listings/ch09-02-dces-minimal/listing-02-01/src/main.rs:All}}
```

<span class="caption">Listing 1-2: dces_minimal - Erstellt eine World, eine entity und ein print systems.</span>

### Kompilierung und Aufruf im zwei Schritten

Die mit **cargo** übersetzte App `dces_minimal` wird im
Unterverzeichnis `target` des Projekt-Wurzelverzeichnis abgespeichert.

```console
$ cargo build --release --bin dces_minimal.rs
$ ../target/release/dces_minimal
```

Auf Windows Systemen musst Du `backslash` als Pfad Trennzeichen nutzen:

```powershell
> cargo build --release --bin dces_minimal.rs
> ..\target\release\dces_minimal.exe
```
