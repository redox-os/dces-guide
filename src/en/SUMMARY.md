# Summary

[Welcome to DCES](title-page.md)
[Introduction](ch00-00-introduction.md)

# Getting Started

- [Getting Started](ch01-00-getting-started.md)
	- [Installation](ch01-01-installation.md)
	- [The Ingredients](ch01-02-ingredients.md)

- [DCES Examples](ch09-00-dces-examples.md)
	- [Basic](ch09-01-dces-basic.md)
	- [Minimal](ch09-02-dces-minimal.md)
	- [Resources](ch09-03-dces-resources.md)
	- [Shared-Components](ch09-04-dces-shared.md)

- [Appendix](appendix-00.md)
	- [A - Keywords](appendix-01-keywords.md)
	- [B - Other Resources](appendix-02-resources.md)
	- [C - Translations](appendix-03-translation.md)
