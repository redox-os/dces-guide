## Installation

Der erste Schritt, ist die Installation von Rust. Dies wird im
Folgenden ausführlich beschrieben [Rust-Buch Kapitel
1](https://github.com/rust-lang/book/blob/master/src/ch01-01-installation.md)

Wenn wir eine DCES-Anwendung erstellen, definieren wir die benötigten
Abhängigkeiten in der Datei Cargo.toml unseres Projekts. Der
Kompiliervorgang löst die Referenzen auf und lädt den Quellcode nach
Bedarf herunter.

> ### Kommandozeilen-Notation
>
> In diesem Kapitel und im gesamten Buch werden wir einige Befehle
> zeigen, die im Terminalfenster erscheinen. Zeilen, die Du in einem
> Terminalfenster eingeben solltest, beginnen alle mit `$`. Du
> brauchst das Zeichen "$" nicht einzugeben; es visualisiert einfach
> den Beginn jedes Befehls. Zeilen, die nicht mit "$" beginnen,
> zeigen normalerweise die Ausgabe des vorherigen Befehls. Außerdem
> wird in PowerShell-spezifischen Beispielen ">" anstelle "$"
> verwendet.

### Fehlersuche


> ***WIP***: Auflistung der häufigsten Probleme? Können wir
> grundlegender Lösungen bereitstellen?

### Lokale Dokumentation

DCES bietet die Möglichkeit, die Dokumentation lokal zu installieren,
so dass Sie sie offline lesen können.

Immer, wenn ein Typ, eine Funktion, eine Methode oder eine Komponente
(crate) vom Toolkit referenziert wird und Du Dir nicht sicher bist,
was dieser bzw. diese tut, oder wie er bzw. es zu verwenden ist, werfe
einen Blick auf die Dokumentation der Programmierschnittstelle
(API) um es herauszufinden!

<!-- [API documentation]: https://www.redox-os.org/orbtk/doc/en -->
<!-- [API documentation]: https://github.com/redox-os/rust-dces -->
[API documentation]: https://docs.rs/rust-dces

### Install Rust on Linux or macOS

Wenn Du Linux oder macOS verwendest, öffne bitte ein Terminal und
Copy/Paste den nachfolgenden Text. Über die Tastatur bestätigst du mit
**Enter** die Aktivierung des Befehls:

```bash
curl https://sh.rustup.rs -sSf | sh
```

### Installation von Rust auf Windows

Laden zunächst das Windows Rust windows Installationsprogramm, das du
auf https://www.rust-lang.org/tools/install findest.

### Installion von  Redoxer (Redox OS)

Wenn Du deine Anwendung in einem
[KVM](https://en.wikipedia.org/wiki/Kernel-based_Virtual_Machine)
kompatiblen OS für Redox erstellen und bauen willst, kannst Du
[redoxer](https://gitlab.redox-os.org/redox-os/redoxer) verwenden.

Zunächst wird die toolchain installiert. Anschließend kannst Du ein
Terminal öffnen und den nachfolgenden Text via Copy/Paste einfügen.
Über die Tastatur bestätigst du mit **Enter** die Aktivierung des
Befehls:

```bash
cargo install redoxer
```

### Editoren und IDE Integration

A wide range of editors and IDE's are providing support for Rust code like

* like syntax-highlighting
* auto-completion
* linting
* lsp support

#### VS Code

Es existiert eine große Gemeinschaft von Anwendern die ihre
Code-Erstellung über VisualStudio Implementierungen verwalten.
Nachfolgende Schritte sind erforderlich, um Deine Installation um die
Komponente `VS Code for Rust` zu ergänzen:

1. Download von [VS Code](https://code.visualstudio.com/download).
2. Installation von [Rust Language Server plugin](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust) (the
   Rust Language Server).

#### Alternative Editoren und IDEs

Wenn Du andere Lösungen bevorzugst, wirst Du wertvolle Information
hoffentlich über die nachfolgenden unvollständige Liste von Links finden:

* [Atom](https://atom.io/packages/language-rust)
* [Intellij IDEA](https://intellij-rust.github.io)
* [Vim](https://github.com/rust-lang/rust.vim)
* [Emacs](https://github.com/rust-lang/rust-mode)
* [Eclipse](https://github.com/eclipse/corrosion)
