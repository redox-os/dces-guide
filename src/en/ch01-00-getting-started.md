# Getting Started

Let’s start your DCES journey! There’s a lot to learn, but every journey starts
somewhere. In this chapter, we’ll discuss:

* Installing DCES on Linux, Bsd, macOS or Windows.
* Using `cargo`, Rust’s package manager and build system.
