// ANCHOR: All
// ANCHOR: Use
use dces::prelude::*;
// ANCHOR_END: Use

#[derive(Default)]
struct Name {
    value: String,
}

struct PrintSystem;

// ANCHOR: Implement_PrintSystem
impl System<EntityStore> for PrintSystem {
    fn run(&self, ecm: &mut EntityComponentManager<EntityStore>, _: &mut Resources) {
        let (e_store, c_store) = ecm.stores();

        // ANCHOR: Query_EntityStore
        for entity in &e_store.inner {
            // ANCHOR: Print_ComponentValue
            if let Ok(comp) = c_store.get::<Name>("name", *entity) {
                println!("{}", comp.value);
            }
            // ANCHOR_END Print_ComponentValue
        }
        // ANCHOR_END: Query_EntityStore
    }
}
// ANCHOR_END: Implement_PrintSystem

// ANCHOR: Main
fn main() {
    let mut world = World::from_entity_store(EntityStore::default());

    world
        .create_entity()
        .components(
            ComponentBuilder::new()
                .with(
                    "name",
                    Name {
                        value: String::from("DCES"),
                    },
                )
                .build(),
        )
        .build();

    world.create_system(PrintSystem).build();
    world.run();
}
// ANCHOR_END: Main
// ANCHOR_END: All
