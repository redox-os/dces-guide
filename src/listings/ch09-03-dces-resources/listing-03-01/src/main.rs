// ANCHOR: All
// ANCHOR: Use
use dces::prelude::*;
// ANCHOR_END: Use

#[derive(Default)]
struct Name {
    value: String,
}

// ANCHOR: StructResource
struct HelloDCES;
// ANCHOR_END: StructResource

// ANCHOR: Implement_Resource
impl HelloDCES {
    pub fn say_hello(&self) -> &str {
        return "Hello DCES!";
    }
}
// ANCHOR_END: Implement_Resource

struct PrintSystem;

// ANCHOR: Implement_PrintSystem
impl System<EntityStore> for PrintSystem {
    // ANCHOR: Run_EntityStore
    fn run(&self, ecm: &mut EntityComponentManager<EntityStore>, res: &mut Resources) {
        let (e_store, c_store) = ecm.stores();

        for entity in &e_store.inner {
            if let Ok(comp) = c_store.get::<Name>("name", *entity) {
                println!("{}", comp.value);
            }
        }

        // ANCHOR: Print_StructResource
        println!("{}", res.get::<HelloDCES>().say_hello());
        // ANCHOR_END: Print_StructResource
    }
    // ANCHOR_END: Run_EntityStore
}
// ANCHOR_END: Implement_PrintSystem

// ANCHOR: Main
fn main() {
    let mut world = World::from_entity_store(EntityStore::default());

    world
        .create_entity()
        .components(
            ComponentBuilder::new()
                .with(
                    "name",
                    Name {
                        value: String::from("DCES"),
                    },
                )
                .build(),
        )
        .build();

    // ANCHOR: Resources
    world.resources_mut().insert(HelloDCES);
    // ANCHOR_END: Resources

    // ANCHOR: System
    world.create_system(PrintSystem).build();
    // ANCHOR_END: System

    // ANCHOR: Run
    world.run();
    // ANCHOR_END: Run
}
// ANCHOR_END: Main
// ANCHOR_END: All
